<?php

namespace App\Controller;

use App\Entity\Post;
use App\Form\PostType;
use App\Repository\ComentarioRepository;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 * @Route("/post", name="post/")
 */
class PostController extends AbstractController
{

    /**
     * @Route("/create", name="create")
     * @param Request $request
     * @param SluggerInterface $slugger
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws Exception
     */
    public function createPost(Request $request,  SluggerInterface $slugger)
    {
        $post = new Post();
        $form = $this->createForm(PostType::class, $post);
        return $this->guardarPost($request,$form,$post, $slugger);
    }

    /**
     * @Route("/ver/{id}", name="ver")
     * @param Post $post
     * @param Request $request
     * @param ComentarioRepository $comRep
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function verPost(Post $post, Request $request, ComentarioRepository $comRep)
    {
        return $this->render('post/verPost.html.twig', [
            'post' => $post,
            'comentarios' => $comRep->findByPost($post),
        ]);
    }

    /**
     * @Route("/editar/{id}", name="editar")
     * @param Post $post
     * @param Request $request
     * @param SluggerInterface $slugger
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws Exception
     */
    public function editarPost(Post $post, Request $request, SluggerInterface $slugger)
    {

        $form = $this->createForm(PostType::class, $post);
        return $this->guardarPost($request,$form,$post,$slugger);

    }

    /*
     * FUNCIÓN COMÚN PARA EDITAR Y CREAR UN POST
     */
    private function guardarPost(Request $request,FormInterface $form, Post $post,  SluggerInterface $slugger){
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $user = $this->getUser();
            $post->setUser($user);
            $post->setFechaPublicacion(new \DateTime());
            $post->setComentarios([]);

            /** @var UploadedFile $brochureFile */
            $brochureFile = $form->get('fotoFile')->getData();

            if ($brochureFile) {
                $originalFilename = pathinfo($brochureFile->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename . '-' . uniqid() . '.' . $brochureFile->guessExtension();

                try {
                    $brochureFile->move(
                        $this->getParameter('fotos_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    throw new Exception("Error cargando la imagen...");
                }

                $post->setFoto($newFilename);
            }

            $em = $this->getDoctrine()->getManager();

            if($post->getId() == null) {
                $em->persist($post);
            }

            $em->flush();
            $this->addFlash('exito', 'se ha guardado el post correctamente');
            return $this->redirectToRoute('dashboard');
        }

        return $this->render('post/formPost.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/eliminar/{id}", name="eliminar")
     */
    public function eliminarPost(Post $post, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();
        return $this->redirectToRoute('dashboard');
    }

}
