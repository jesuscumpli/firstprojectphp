<?php

namespace App\Controller;

use App\Repository\PostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractController
{
    /**
     * @Route("/dashboard", name="dashboard")
     */
    public function index(PostRepository $post_rep)
    {
        $user = $this->getUser();
        $posts = $post_rep->findAll();

        return $this->render('dashboard/index.html.twig', [
            'user' => $user,
            'posts' => $posts,
        ]);
    }
}
