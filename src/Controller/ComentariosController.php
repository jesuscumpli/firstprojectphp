<?php

namespace App\Controller;

use App\Entity\Comentario;
use App\Entity\Post;
use App\Repository\ComentarioRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/comentario", name="com/")
 */
class ComentariosController extends AbstractController
{
    /**
     * @Route("/comentar/{id}", name="comentar")
     * @param Request $request
     * @param Post $post
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function comentar(Request $request, Post $post)
    {

        $comentario = new Comentario();
        $message = $request->get("mensaje");
        $user = $this->getUser();

        //Setters
        $comentario ->setUser($user);
        $comentario->setComentario($message);
        $comentario->setFechaPublicacion(new \DateTime());
        $comentario->setPost($post);

        //Database
        $em = $this->getDoctrine()->getManager();
        $em->persist($comentario);
        $em->flush();

        return $this->redirectToRoute("post/ver", array('id' => $post->getId()));
    }

    /**
     * @Route("/eliminar/{id}", name="eliminar")
     * @param Comentario $com
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function eliminarComentario(Comentario $com)
    {
        $post = $com->getPost();

        //Borrar de la Base de Datos
        $em = $this->getDoctrine()->getManager();
        $em->remove($com);
        $em->flush();

        return $this->redirectToRoute("post/ver", array('id' => $post->getId()));
    }
}
