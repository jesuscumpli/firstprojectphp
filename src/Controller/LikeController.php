<?php

namespace App\Controller;

use App\Entity\Post;
use App\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/like", name="like/")
 */
class LikeController extends AbstractController
{

    /**
     * @Route("/like/{id}", name="like")
     * @param Post $post
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function like(Post $post)
    {
        /** @var \App\Entity\User $user */
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        //Obtener listas actuales
        /** @var ArrayCollection */
        $users = $post->getUserLiked();
        /** @var ArrayCollection */
        $posts = $user->getPostsLiked();

        //Añadir y actualizar las lista liked de post y usuario
        $users->add($user);
        $post->setUserLiked($users);
        $posts->add($post);
        $user->setPostsLiked($posts);

        //Guardar en Base de datos
        $em->flush();

        return $this->redirectToRoute("post/ver", array('id' => $post->getId()));
    }

    /**
     * @Route("/dislike/{id}", name="dislike")
     * @param Post $post
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function dislike(Post $post)
    {
        /** @var \App\Entity\User $user */
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        //Obtener listas actuales
        /** @var ArrayCollection */
        $users = $post->getUserLiked();
        /** @var ArrayCollection */
        $posts = $user->getPostsLiked();

        //Eliminar y actualizar las lista liked de post y usuario
        $users->removeElement($user);
        $post->setUserLiked($users);
        $posts->removeElement($post);
        $user->setPostsLiked($posts);

        //Guardar en Base de datos
        $em->flush();

        return $this->redirectToRoute("post/ver", array('id' => $post->getId()));
    }
}
